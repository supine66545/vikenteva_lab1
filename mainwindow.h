#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_spinMin_valueChanged(int arg1);

    void on_spinMax_valueChanged(int arg1);

    void on_editPushAdd_clicked();

    void on_editPushDelete_clicked();

    void on_editPushClear_clicked();

    void on_calcPushClearAll_clicked();

    void on_calcPushCalc_clicked();

    void on_calcPushCalc_2_clicked();

    void on_calcPushCalc_3_clicked();

private:
    Ui::MainWindow *ui;
    QStandardItemModel *model;

};
#endif // MAINWINDOW_H
